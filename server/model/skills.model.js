/*
 File: skillsfuture.model.js in the root/model folder
 Define:
 1. The talent_future Table Model
    Primary Key: id
    creator_id link to the user database
*/

var Sequelize = require("sequelize");
module.exports = function (database) {
    var SkillsModel =
        database.define("skills_future_costs", {
            url: {type: Sequelize.STRING},
            id: {type: Sequelize.STRING, primaryKey: true},
            coursetext:{type: Sequelize.STRING},
            courseblock:{type: Sequelize.STRING},
            coursecost: {type: Sequelize.INTEGER},
            comments: {type: Sequelize.TEXT}
        }, {
            timestamps: true
        });

    return SkillsModel;
};
