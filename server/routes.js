
'use strict';

const express = require("express");

const path = require("path");
const docroot = path.join(__dirname, "..");
const CLIENT_FOLDER = path.join(docroot, "client");
const BOWER_FOLDER = path.join(docroot, "bower_components");

const UsersController = require("./api/user/user.controller");
const SkillsController = require("./api/skillsfuture/skills.controller");
const AWSController = require("./api/aws/aws.controller");

module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
};


function configureRoutes(app){
   //  app.post("/api/users", UsersController.addUser);
   // app.get("/api/userlist", UsersController.getUserList);
    // app.put("/api/login", UsersController.updateLogin);

    // app.get("/api/skills", SkillsController.listAll);
    //app.put("/api/skills/likes", skills.updateLikes);

    app.post("/api/user", UsersController.addUser);
    // app.put("/api/user", UsersController.updateUser);
    // app.get("/api/userlist/:name" , UsersController.getUser);
    // app.get("/api/userlist1" , UsersController.getUser);
    // // app.get("/api/userlist", UsersController.listUsers);
    // app.get("/api/user/:userId", UsersController.getUserInfo);

   // app.post("/api/skills/upload", SkillsController.createSkills);
    app.get("/api/aws/url", AWSController.getCloudURL);
    app.post("/api/aws/s3-policy", AWSController.getSignedPolicy);

    app.use(express.static(CLIENT_FOLDER));
    app.use("/bower_components",express.static(BOWER_FOLDER));
//    console.log(CLIENT_FOLDER);
//    console.log(BOWER_FOLDER);
}

function errorHandler(app) {
    //app.use(function (req, res) {
      //  console.log("Client Error 400", req.headers);
        //res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    //});

    app.use(function (err, req, res, next) {
        console.log("Server Error 500",err);
        //res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
}



