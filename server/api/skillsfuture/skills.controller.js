//to read to Skills_future_costs SQL Table

'use strict';

var SkillsModel = require('../../database').skills_future_costsTable;

exports.addSkills = function (req, res) {
    SkillsModel.create(
        {
            name: req.body.name,
            email: req.body.email,
            contact: req.body.contact,
            gender: req.body.contact,
            //skills: req.body.skills
        })
        .then(function (results) {
            console.log(results);
            res.status(200).send("Record Added");
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).send("Fail to add record");

        });
};

exports.listskills = function (req, res) {
    SkillsModel.findAll()
        .then(function (response) {
            console.log("listskills called");
            res.json(response);
        }).catch(function (error) {
        res.status(500).send({'error': error});
    });
};
exports.getSkills = function (req, res) {
    var where = {};
    //where.name = req.query.name;
    // where.name = {$like: "%" + req.query.name + "%"};
    where.name = {$like: "%" + req.params.name + "%"};

    console.log("Before getSkills value: " + JSON.stringify(req.query));

    SkillsModel.findAll({
        where : where
    })
        .then(function (response) {
            console.log("getSkills value: " + req.query.name);
            res.json(response);
        })
        .catch(function (error) {
            res.status(500).send({'error': error});
        });
}

exports.getSkillsInfo = function (req, res) {
    console.log("getSkillsInfo", req.query, req.body, req.params);
    SkillsModel.findOne({
        where: {id: req.params.userId},
        attributes: ['id','name', 'email', 'contact', 'gender']
    })
        .then(function (response) {
            console.log("retrieved record from server");
            res.json(response);
        }).catch(function (error) {
        res.status(500).send({'error':error});
    });
};


exports.updateSkills = function (req, res) {
    var where = {};
    where.id=req.body.id
    SkillsModel.update(
        {
            name: req.body.name,
            email: req.body.email,
            contact: req.body.contact,
            gender: req.body.gender,

                    },
    {where: where})

        .then(function (results) {
            console.log(results);
            res.status(200).send("Record updated");
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).send("Fail to update record");

        });
};


