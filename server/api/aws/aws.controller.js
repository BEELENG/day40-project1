// Reading:
// Uploads to S3 using HTML POST Forms, https://aws.amazon.com/articles/1434/
var config = require('../../config'),
    crypto = require('crypto');

const EXPIRY_IN_MINUTES = 2;

var getExpiryTime = function () {
    var date = new Date();
    date.setMinutes(date.getMinutes() + EXPIRY_IN_MINUTES);
    return date.toISOString();
};

var signPolicy = function (base64Policy) {
    return crypto
        .createHmac('sha1', config.aws.key)
        .update(new Buffer(base64Policy, 'utf-8'))
        .digest('base64');
};

// The policy will limit to upload up to 1MB file

var getS3Policy = function (contentType) {
    return {
        'expiration': getExpiryTime(),
        'conditions': [
            {'bucket': config.aws.bucket},
            {'acl': 'public-read'},
            ['starts-with', '$key', ""],
            ["starts-with", "$Content-Type", ""],
            ["starts-with", "$filename", ""],
            ["content-length-range", 0, 1048576]
        ]
    };
};

var createS3Policy = function (contentType) {
    var s3Policy = getS3Policy(contentType);
    var stringPolicy = JSON.stringify(s3Policy);
    var base64Policy = new Buffer(stringPolicy, 'utf-8').toString('base64');
    var signature = signPolicy(base64Policy);

    return {
        s3Policy: base64Policy,
        s3Signature: signature,
        AWSAccessKeyId: config.aws.id,
        bucketUrl: config.aws.url

    };
};

exports.getCloudURL = function(req, res) {
    res.status(200).json({'url': config.aws.url});
};

exports.getSignedPolicy = function (req, res) {
    var signedPolicy = createS3Policy(req.body.mimeType);
    res
        .status(200)
        .json(signedPolicy);
};