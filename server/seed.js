/* Data Seeding */

var Sequelize = require('sequelize');

var database = new Sequelize('talent_future', 'root', 'myrootpwd', {
    host: "localhost",
    pool: {min: 0, max: 4, idle: 1000}
});

database.authenticate()
    .then(function (result) {
        console.log("OK");
    })
    .catch(function (err) {
        console.log("NOK", err);
    });

/* Seed Data for skills & users */
var SkillsModel = require('./model/skills.model')(database);
var UsersModel = require('./model/users.model')(database);

database.sync({
    logging: console.log,
    force: true
}).then(function () {

    console.log("Database is in Sync");

    SkillsModel.create(
        {
            // "url": "/courseblock(organiser)/a.jpg",
            "creator_id": "1",
            "url": "/skills/a.jpg",
            "coursetext": "",
            "courseblock": "",
            "coursecost": "",
            "comments": ""
        });

    SkillsModel.create(
        {
            // "url": "/courseblock(organiser)/b.jpg",
            "creator_id": "2",
            "url": "/skills/b.jpg",
            "coursetext": "",
            "courseblock": "",
            "coursecost": "",
            "comments": ""
        });

    SkillsModel.create(
        {
            // "url": "/courseblock(organiser)/c.jpg",
            "creator_id": "3",
            "url": "/skills/c.jpg",
            "coursetext": "",
            "courseblock": "",
            "coursecost": "",
            "comments": ""
        });


    UsersModel.bulkCreate([
        {
            name: "Tan Wai Yin",
            email: "WaiYinTan@Yahoo.com.sg",
            contact: "88610001",
            Gender: "Female"
        },
        {
            name: "Sharon Wong",
            email: "SharonW@gmail.com",
            contact: "96793003",
            Gender: "Female",
        },
        {
            name: "Justin Barker",
            email: "Justin.Barker@gmail.com",
            contact: "76581007",
            Gender: "Male",
        },
        {
            name: "Bob Menon",
            email: "RaviMenon@yahoo.com",
            contact: "96971334",
            Gender: "Male",
        }

    ]).then(function () {
        console.log("Bulk Create OK")
    }).else(function (error) {
        consol.log(error)
    })

}).
    catch(function (Error) {
        console.log("Error in data creation");
        console.log(Error)
    });