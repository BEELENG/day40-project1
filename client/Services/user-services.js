'use strict';

(function () {
    angular
        .module("TalentApp")
        .service("UserServices", UserServices);

    UserServices.$inject = ["$http", "$q"];

    console.log("Entered User Services");
    function UserServices($http, $q) {
        var obj = {};

        obj.addUser = function(userObject) {
            var deferred = $q.defer();
            $http
                .post("/api/user",userObject)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;
        };

        obj.listUsers = function() {
            var deferred = $q.defer();
            $http
                .get("/api/userList")
                .then(function (response) {
                    console.log("listUser-services log ");
                    deferred.resolve(response.data);
                }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        obj.getUser = function (Username) {
            var deferred = $q.defer();
            console.log("user-services log : " + username);
            $http
                .get("/api/userList/"+username)
                // .get("/api/userList1", {
                //      params: {'name': username}
                //  })

                .then(function (response) {
                    console.log("Success: " + response.data);
                    deferred.resolve(response.data);
                }).catch(function (error) {
                     deferred.reject(error.data);
                 });
            return deferred.promise;
        };

        obj.updateLoginUser = function(User) {
            var deferred = $q.defer();
            console.log("calling REST API to login user");
            $http
                .put("/api/login", user)
                .then(function (response) {
                    console.log("success http login", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http login", response);
                deferred.reject(response);
            });
            return deferred.promise;
        };

        obj.getUserList = function() {
            var deferred = $q.defer();
            console.log("calling REST API to get user list");
            $http
                .get("/api/userList")
                .then(function (response) {
                    console.log("success http userList", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http userList", response);
                deferred.reject(response);
            });
            return deferred.promise;
        };
        
        return obj;
    }
})();

