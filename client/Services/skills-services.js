
'use strict';

(function () {
    angular
        .module("TalentApp")
        .service("SkillServices", SkillServices);

    SkillServices.$inject = ["$http", "$q"];

    //console.log("Entered Skill Services");
    function SkillServices($http, $q) {
        var obj = {};

        obj.getSkillCloudURL = function() {
            var deferred = $q.defer();
            $http
                .get("/api/aws/url")
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(response){
                    deferred.reject(response.data);
                })
            return deferred.promise;
        };


        obj.ListSkills = function() {
            var deferred = $q.defer();
            $http
                .get("/api/skills")
                .then(function (response) {
                    deferred.resolve(response.data);
                }).catch(function (response) {
                deferred.reject(response.data);
            });
            return deferred.promise;
        };

        obj.updateSkills = function(skill_info) {
            var deferred = $q.defer();
            $http
                .put("/api/skill/skills",skill_info)
                .then(function (response) {
                    //console.info("success http updateSkills", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                //console.info("error http updateSkills", response);
                deferred.reject(response.data);
            });
            return deferred.promise;
        };

        return obj;
    }
})();
