/*File: SkillsUpload.html
  Purpose: Controller of the skillsUpload page
 */
(function () {
    angular
        .module("TalentApp")
        .controller("SkillUploadCtrl", UploadCtrl);

    UploadCtrl.$inject = ['$http', '$element', 'Upload', 'close'];

    function UploadCtrl($http, $element, Upload, close) {

        var vm = this;
        vm.file = "";
        vm.caption = "";
        vm.showUpload = true;

        vm.submit = function() {
            prepareUpload(function(url) {save(url)});
        };

        var save = function (url) {
            $http
                .post("/api/skills/upload", {
                    url: url,


                    comment: vm.caption
                })
                .then(function (response) {
                    // new skill record - use to add skill to skill table feed??
                    console.log(response.data);
                    vm.file = "";
                    vm.caption = "";
                    vm.showUpload = false;
                    //????
                    $element.modal('hide');
                    close(null, 200)
                })
                .catch(function () {
                    alert("Oops some error occurred - save");
                });
        };

        var prepareUpload = function (afterUpload) {
            console.log("PrepareUpload");
            $http
                .post("/api/aws/s3-policy", {
                    mimeType: vm.file.type
                })
                .then(function (response) {
                    uploadFile(response.data, afterUpload);
                })
                .catch(function (response) {
                    console.log("PreparedUpload Error!");
                });
        };

        var uploadFile = function (fileUploadConfig, afterUpload) {
            Upload.upload({
                url: fileUploadConfig.bucketUrl,
                method: 'POST',
                data: {
                    key: "uploads/" + Date.now() + vm.file.name,
                    AWSAccessKeyId: fileUploadConfig.AWSAccessKeyId,
                    acl: "public-read",
                    policy: fileUploadConfig.s3Policy,
                    signature: fileUploadConfig.s3Signature,
                    "Content-Type": vm.file.name,
                    filename: vm.file.name,
                    file: vm.file
                }
            }).then(function (resp) {
                afterUpload(resp.config.data.key);
            }, function (resp) {
                // Called when upload fails
                console.log("Upload to AWS fail");
                console.log(">>>>" + resp.config.data);
                alert("Sorry, we could not upload the skill programme. Check the programme details!")
            }, function (evt) {
                // Called as the upload progresses
            });
        };


    }

})();



