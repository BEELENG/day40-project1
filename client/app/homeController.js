
'use strict';
(function () {
    angular
        .module("TalentApp")
        .controller("HomeCtrl", HomeCtrl);

    HomeCtrl.$inject = ['ModalService', "UserServices"];

    function HomeCtrl(ModalService, UserServices) {
        var vm = this;

        // exposed methods
        vm.showUploadWindow = showUploadWindow;
        vm.userList = getUserList;
        vm.login = setLoggedInUser;

        vm.loggedin_user = {};
        vm.userList = [];

        console.log("Entered Home Controller");

        function showUploadWindow() {
            console.log("showUploadWindow");
            ModalService.showModal({
                templateUrl: "../views/index.html",
                controller: "SkillsUploadCtrl as uploader",
                inputs: {
                    title: "SkillsFuture"
                }
            }).then(function (modal) {
                console.log("modal Service OK");
                modal.element.modal();
                modal.close.then(function (result) {

                });
            });
        }

        function setLoggedInUser() {
            console.log("New User logged in", vm.loggedin_user);
            //vm.loggedin_user = vm.userList[vm.loggedin_userIndex];
            // inform server to update session info
            UserServices.updateLoginUser(vm.loggedin_user)
                .then(function (result) {
                    console.log("User login registered to server", result);
                })
                .catch(function (result) {
                    console.log("Server failed to login user", result);
                });
        }

        function getUserList() {
            console.log("calling User Services to get user list");
            UserServices.getUserList()
                .then(function (result) {
                    vm.userList = result.userlist;
                    vm.loggedin_user = result.loggedin_user;

                    /*// find index of current loggedin user within the user list
                     vm.userList.forEach(function(item, index) {
                     if (vm.loggedin_user.id == item.id) {
                        vm.loggedin_userIndex = index;
                     }
                     });*/

                    console.log("HomeCtrl received user list", vm.userList);
                    console.log("Logged in user" + vm.loggedin_user );
                })
                .catch(function (result) {
                    console.log("No user list ", result);
                });
        }
        getUserList();
    }
})();

