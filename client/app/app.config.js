'use strict';
(function () {
    angular
        .module("TalentApp")
        .config(TalentAppConfig);

    TalentAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function TalentAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('A',{
                url : '/A/:user_id',
                templateUrl :'../views/talent.html',
                controller : 'TalentCtrl',
                controllerAs : 'vm'
            })
            .state('B', {
                url: '/B/:user_id',
                templateUrl: '../views/skills.html',
                controller: 'SkillsCtrl',
                controllerAs: 'myCtrl'
            })
            .state('C',{
                url : '/C/:user_id',
                templateUrl :'../views/namelist.html',
                controller : 'UsersCtrl',
                controllerAs : 'myCtrl'
            });

        $urlRouterProvider.otherwise("/A/");
    }
})();